+++
title = 'CSP复赛-指南'
date = 2023-10-16T00:00:00+08:00
categories = ['CSP']
draft = false

+++
一、注意事项：



1. 考生必须按照所在省份下发的第二轮认证通知中的要求参加认证，未按要求作答会导致0分。

2. 参加考试过程中，严格按照考场要求参加考试，遵守考场纪律，听从监考老师安排。

3. 有任何关于考试的问题，切勿自行处理，务必第一时间向监考老师寻求帮助。

4. CSP 第二轮认证会采用 NOI-Linux 2.0 进行判卷，请考生在考前阅读各省通知，错误操作导致0分。

5. Windows 环境下的一些语句可能在Linux环境下会导致编译不通过，其中包括但不限于：

    1. Windows环境下的__int64类型在Linux环境下无法通过编译。

    2. 输入文件中Windows环境下换行符与Linux环境下换行符的表示方式不同。

    3. 如果用scanf和printf语句时，注意要用"%lld"，或用流输入输出。



二、文件相关



1. 文件操作代码模板：



```cpp

#include <iostream>

#include <cstdio>

using namespace std;



// Some codes



int main() {

    freopen("filename", "r", stdin); // Requires change

    freopen("filename", "w", stdout); // Requires change



    // Some codes



    fclose(stdin);

    fclose(stdout);

    return 0;

}



```



1. 需检查的文件名：

    1. 测试文件名(通常显示在考试卷内)

    2. 题目文件夹名(通常显示在考试卷内)

    3. 个人文件夹名(通常为准考证)

2. 考试卷解读

3. （主要关注重点）题目概览

    1. （非关注重点）题目名称：指的是题目标题。

    2. （次要关注重点）题目类型：指题目的作答类型，通常为传统型。

    3. （主要关注重点）目录：目录中提供的名称就是考生需要创建的题目文件夹名。

    4. （主要关注重点）可执行文件名：考生编程时所创建的题目文件名。

    5. （主要关注重点）输入文件名：文件操作中`freopen("输入文件名", "r", stdin);`。

    6. （主要关注重点）输出文件名：文件操作中`freopen("输出文件名", "w", stdout);`。

    7. （次要关注重点）每个测试点时限：代表程序每个测试点的最长运行时长。

    8. （次要关注重点）内存限制：代表程序能获取的最大内存大小。

    9. （非关注重点）测试点数目：评测中提供的测试点数目。

    10. （非关注重点）测试点是否等分：所有的测试点所占是否分数相等。

4. （主要关注重点）提交源程序文件名：指在题目文件夹中的C++文件名。

5. （次要关注重点）编译选项：指判卷时索使用测试机的C++编译器设置。

6. （主要关注重点）注意事项：请各位考生严格遵守注意事项中的要求。



三、各省信息



第二轮比赛各省份的考试要求有所区别，



请各位考生参考：[https://www.noi.cn/gs/xw/](https://www.noi.cn/gs/xw/)，



各省份会在第二轮考试前发布第二轮竞赛相关信息。



四、错误相关



1. 建议将数组都定义为全局变量，以防止数组过大出现空间复杂度过高的问题。

2. 编译错误处理：

    1. 寻找错误的方法：编译器会给出错误所在的大概位置。

    2. 未定义：（`'name' was not declared in this scope.`）

        1. 变量未定义、函数未声明。

        2. 头文件未引入。

    3. 括号不匹配：（`expected '{' at end of input`）

        1. 一层一层寻找括号。

    4. 行末缺少分号：（`expected ';' before 'name'`）

        1. 在给出第(给出的大概行数-1)行寻找。

3. 逻辑错误处理：

    1. 死循环：在循环后写一个输出，如输出从未执行，则说明死循环。

    2. 使用了不存在的数组下标：检查即可。

    3. 变量未重置：检查即可。

    4. 变量或容器未初始化：检查即可。



五、骗分小妙招



1. 暴力枚举骗分法

遇事不决就暴力，按题目要求进行打表或枚举，优先保证小数据可过。

2. 边界值骗分法

对于一些模拟、递推类的问题，通过特判直接输出边界的结果，优先保证边界数据可过。

3. 大数据骗分法

不要用int！不要用int！不要用int！用long long。

不要用double！不要用double！不要用double！用long double。