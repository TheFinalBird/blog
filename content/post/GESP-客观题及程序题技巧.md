+++
title = 'GESP-客观题及程序题技巧'
date = 2023-12-03T00:00:00+08:00
categories = ['GESP']
draft = false

+++
1. 客观题题目类型：

    1. 基础知识型

        1. 控制回答时间为30~60秒。

        2. 结合经验，总结答案，勿超时间。

    2. 公式计算型

        1. 控制回答时间为3~5分钟。

        2. 拿出稿纸，进行演算，勿超时间。

2. 看懂题目，提取题目关键信息

3. 为确保某题是否可得满分，在测试程序时一定测试：

    1. 样例

    2. 2~3组手工数据

    3. 边界数据

    4. 特例数据（针对特殊判断设计的数据）

    5. 无解数据（针对无解情况设计的数据）

4. 能则做，会则做：

    1. 无法解题，则输出样例数据。

    2. 解一部分，则输出部分结果。

    3. 用暴力法，则尝试进行优化。

    4. 用数学法，则考虑边界数据。

5. 优化：

    1. 使用scanf/printf或关闭cout/cin的绑定，具体方法：`cin.tie(nullptr);cout.tie(nullptr);`。

    2. 保留小数尽量使用printf。

    3. 考虑高精度问题。

    4. 排序尽量使用sort函数，头文件为`<algorithm>`

    5. 定义数组尽量使用全局定义、容量比题目数据范围大一些。

    6. 数学函数无需手写，头文件为`<cmath>`

    7. 定义变量及数组要考虑初始值。