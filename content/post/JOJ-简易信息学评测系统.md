+++
title = 'JOJ-简易信息学评测系统'
date = 2024-02-04T00:00:00+08:00
categories = ['分享']
draft = false

+++
~~最近把烂尾的Online-Judge 项目救了回来，~~



~~接着发现挺好用。~~



JOJ=Jun-Online-Judge



JOJ 的好处：简易、安全、高效



这样说可能不能体现出 JOJ 的好处，



打分表格（满分 10 分）：



（JOJ 与其他 OJ 比较，个人观点，不喜勿喷）

<table>
    <tr>
        <td></td>
        <td>JOJ</td>
        <td>Hydro</td>
        <td>UOJ</td>
    </tr>
    <tr>
        <td>沙盒系统</td>
        <td>6</td>
        <td>8</td>
        <td>7</td>
    </tr>
    <tr>
        <td>搭建简易</td>
        <td>9</td>
        <td>8.5</td>
        <td>8</td>
    </tr>
    <tr>
        <td>占用空间小</td>
        <td>9</td>
        <td>7</td>
        <td>5</td>
    </tr>
</table>


目前已实现及会实现的功能：



- [x]  用户系统

- [x]  比赛系统（IOI 赛制）

- [x]  一键备份还原

- [x]  排行榜系统

- [x]  个人资料页面

- [x]  一键安装



一键安装：



```bash

curl https://raw.githubusercontent.com/Jun-Software/Jun-Online-Judge/master/install.sh | sudo bash

```



下载地址及部署教程：



[June / Jun-Online-Judge · GitLab](https://gitlab.com/TheFinalBird/Jun-Online-Judge)