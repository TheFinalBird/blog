+++
title = 'Vercel-Cloudflare SSL无效'
date = 2023-09-02T00:00:00+08:00
categories = ['问题']
draft = false

+++
## 事情起因



原本，我在Vercel上搭了个网站，



绑定到Cloudflare的DNS上，打开了CDN，



SSL也配置成了“完全”。



## 问题出现



访问链接，Cloudflare的CDN报错，内容为：



```

Invalid SSL Certificate

```



## 尝试解决



我认为Vercel应该没那么快部署证书，



到了第二天，我再看，还是不能访问，



以前我也是按这种流程操作，是可以成功访问的，



我就查了查Vercel啊，



Vercel文档里说要配置CAA，



配置了，又等了一天，还是不行！



## 问题解决



我就继续查，查到一个[vercel仓库的issues](https://github.com/vercel/vercel/issues/1978)，



这个issue终于解答了我的疑惑，



Cloudflare的CDN需要关闭！