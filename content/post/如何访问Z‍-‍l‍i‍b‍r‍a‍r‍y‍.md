+++
title = '如何访问Z‍-‍l‍i‍b‍r‍a‍r‍y‍'
date = 2023-08-22T00:00:00+08:00
categories = ['教程']
draft = false

+++
有一个国外的免费电子书网站，叫做‍Z‍-‍l‍i‍b‍r‍a‍r‍y‍，

这个网站2022年底因为用户上传‍盗‍版‍电子书被‍封‍禁‍，

2023年宣布回归，

并添加了书籍审核机制。

现可用域名(2023/08/22)：



[‍z‍l‍i‍b‍r‍a‍r‍y‍-‍c‍h‍i‍n‍a‍.‍s‍e‍](https://zlibrary-china.se/)



[‍z‍l‍i‍b‍r‍a‍r‍y‍-‍r‍e‍d‍i‍r‍e‍c‍t‍.‍s‍e‍](https://zlibrary-redirect.se/)



[‍s‍i‍n‍g‍l‍e‍l‍o‍g‍i‍n‍.‍s‍e‍](https://singlelogin.se/)



[‍s‍i‍n‍g‍l‍e‍l‍o‍g‍i‍n‍.‍r‍e‍](https://singlelogin.re/)



如发现新域名，请使用WHOIS查询，如果注册商不包含s‍e‍r‍a‍k字样则为诈骗网站。



官方为了防止用户找域名找的太幸苦，

推出了基于electron和t‍o‍r‍的客户端，

还推出了Chrome和Firefox浏览器扩展。