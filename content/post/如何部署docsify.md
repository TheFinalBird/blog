+++
title = '如何部署docsify'
date = 2023-08-07T00:00:00+08:00
categories = ['教程']
draft = false

+++
首先，



进入[此git库](https://github.com/lemonorangeapple/docsify-example)，



选择*Use this template*，



修改*docs*目录下的markdown文件及内容，



此git仓库提供两种部署方式，



分别是Vercel部署和独立部署。



### Vercel部署



在Vercel导入此仓库并进行部署，



*Build Command*保持默认，



修改*Output Directory*为`docs`，



修改*Install Command*为`npm install`，



点击*Build*部署即可



### 独立部署



安装node.js及npm环境，



使用git克隆仓库，



再运行目录下的serve.bat或serve.sh即可。